This project contains AsciiDoc source for CFEngine 3 (circa 3.7) 
summary reference materials developed by Aleksey Tsalolikhin
and Mike Weilgart of Vertical Sysadmin, Inc. (Based on the materials
of Mark Burgess and CFEngine, AS.)
