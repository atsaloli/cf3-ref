#!/bin/sh

FILE_BASENAME=agent_attributes_351

/home/atsaloli/bin/asciidoc --doctype article  --backend=docbook ${FILE_BASENAME}.txt

xmlto \
-vv --noclean \
--with-dblatex \
--skip-validation \
pdf \
${FILE_BASENAME}.xml

mv ${FILE_BASENAME}.pdf /home/atsaloli/www/cfengine/agent-refcard.pdf
echo PDF done
