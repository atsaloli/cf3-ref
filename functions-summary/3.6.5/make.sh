#!/bin/sh

a2x --doctype article --format pdf \
--xsltproc-opts="--stringparam toc.section.depth 0 --stringparam  section.autolabel 0 --stringparam  generate.toc none "  \
/home/atsaloli/asciidoc-input/configuration_management/functions-summary/functions-summary.txt  && \
mv functions-summary.pdf /home/atsaloli/www/cfengine/ && \
echo PDF done, see http://www.verticalsysadmin.com/cfengine/functions-summary.pdf
exit
# does not seem to work right, still includes TOC
# I manually removed it with "pdftk burst" followed by "pdfjoin" of the pages following the TOC
