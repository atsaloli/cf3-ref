Functions Summary for CFEngine 3.7.1
====================================
Based on the works of Mark Burgess and CFEngine AS.


[cols="30%,70%"]
|=======================================================================================
| accessedbefore | Compares the atime fields of two files.
| accumulated | Convert an accumulated amount of time into a system representation.
| ago | Convert a time relative to now to an integer system representation.
| and | Returns whether all arguments evaluate to true.
| bundlesmatching | Return the list of defined bundles matching name and any tags given. Both bundlename and tags are regular expressions. name is required, tags are optional.
| bundlestate | Returns the current evaluation data state for bundle bundlename.
| canonify | Convert an arbitrary string text into a legal class name.
| canonifyuniquely | Convert an arbitrary string text into a unique legal class name.
| changedbefore | Compares the ctime fields of two files.
| classesmatching | Return the list of set classes matching name and any tags given. Both name and tags are regular expressions. name is required, tags are optional.
| classify | Returns whether the canonicalization of text is a currently set class.
| classmatch | Tests whether regex matches any currently set class.
| concat | Concatenates all arguments into a string.
| countclassesmatching | Count the number of defined classes matching regex.
| countlinesmatching | Count the number of lines in file filename matching regex.
| data_expand | Transforms a data container to expand all variable references.
| data_readstringarray | Returns a data container (map) with up to maxentries-1 fields from the first maxbytes bytes of file filename. The first field becomes the key in the map.
| data_readstringarrayidx | Returns a data container (array) with up to maxentries fields from the first maxbytes bytes of file filename.
| data_regextract | Returns a data container filled with backreferences and named captures if the anchored regex matches the string.
| datastate | Returns the current evaluation data state.
| difference | Returns the unique elements in list1 that are not in list2.
| dirname | Return the parent directory name for given path.
| diskfree | Splits the file filename into separated values and returns the list.
| escape | Escape regular expression characters in text.
| eval | Returns expression evaluated according to mode and options. Currently only the math and class modes with infix option are supported for evaluating traditional math expressions.
| every | Returns whether every element in the variable list matches the unanchored regex.
| execresult | Execute command and return output as string.
| expandrange | Generates a list based on an ordered list of numbers selected from a range of integers, in steps specified by the second argument.
| file_hash | Return the hash of file using the hash algorithm.
| fileexists | Returns whether the file filename can be accessed.
| filesexist | Returns whether all the files in list can be accessed.
| filesize | Returns the size of the file filename in bytes.
| filestat | Returns the requested file field field for the file object filename.
| filter | Transforms a list or data container into a list subset thereof.
| findfiles | Return the list of files that match any of the given glob patterns.
| format | Applies sprintf-style formatting to a given string.
| getclassmetatags | Returns the list of meta tags for class classname.
| getenv | Return the environment variable variable, truncated at maxlength characters
| getfields | Fill array_lval with fields in the lines from file filename that match regex, split on split.
| getgid | Return the integer group id of the group groupname on this host.
| getindices | Returns the list of keys in varref which can be the name of an array or container.
| getuid | Return the integer user id of the named user on this host
| getusers | Returns a list of all users defined, except those names in exclude_names and uids in exclude_ids
| getvalues | Returns the list of values in varref which can be the name of an array or container.
| getvariablemetatags | Returns the list of meta tags for variable varname.
| grep | Returns the sub-list if items in list matching the anchored regular expression regex.
| groupexists | Returns whether a group group exists on this host.
| hash | Return the hash of input using the hash algorithm.
| hashmatch | Compute the hash of file filename using the hash algorithm and test if it matches hash.
| host2ip | Returns the primary name-service IP address for the named host hostname.
| hostinnetgroup | True if the current host is in the named netgroup.
| hostrange | Returns whether the current host lies in the range of enumerated hostnames specified with prefix.
| hostsseen | Returns a list with the information field of hosts that were seen or not seen within the last horizon hours.
| hostswithclass | Returns a list from the CFEngine Database with the information field of hosts on which classs is set.
| hubknowledge | Read global knowledge from the CFEngine Database host by id.
| ifelse | Evaluate each pair of arguments up to the last one as a (class, value) tuple, returning value if class is set.
| intersection | Returns the unique elements in list1 that are also in list2.
| ip2host | Returns the primary name-service host name for the IP address ip.
| iprange | Returns whether the current host lies in the range of IP addresses specified.
| irange | Define a range of integer values for CFEngine internal use.
| isdir | Returns whether the named object filename is a directory.
| isexecutable | Returns whether the named object filename has execution rights for the current user.
| isgreaterthan | Returns whether value1 is greater than value2.
| islessthan | Returns whether value1 is less than value2.
| islink | Returns whether the named object filename is a symbolic link.
| isnewerthan | Returns whether the file newer is newer (modified later) than the file older.
| isplain | Returns whether the named object filename is a plain/regular file.
| isvariable | Returns whether a variable named var is defined.
| join | Join the items of list into a string, using the conjunction in glue.
| lastnode | Returns the part of string after the last separator.
| laterthan | Returns whether the current time is later than the given date and time.
| ldaparray | Fills array with the entire LDAP record, and returns whether there was a match for the search.
| ldaplist | Returns a list with all named values from multiple ldap records.
| ldapvalue | Returns the first matching named value from ldap.
| length | Returns the length of list.
| lsdir | Returns a list of files in the directory path matching the regular expression regex.
| makerule | Evaluates whether a target file needs to be built or rebuilt from one or more sources files.
| maparray | Returns a list with each array_or_container element modified by a pattern.
| mapdata | Returns a data container holding a JSON array. The array is a map across each element of array_or_container, modified by a pattern. The map is either collected literally when interpretation is none or parsed as JSON when interpretation is json.
| maplist | Return a list with each element in list modified by a pattern.
| max | Return the maximum of the items in list according to sortmode (same sort modes as in sort()).
| mean | Return the mean of the numbers in list.
| mergedata | Returns the merger of any named data containers or lists. Can also wrap and unwrap data containers.
| min | Return the minimum of the items in list according to sortmode (same sort modes as in sort()).
| none | Returns whether no element in list matches the regular expression regex.
| not | Calculate whether expression is false
| now | Return the time at which this agent run started in system representation.
| nth | Returns the element of list_or_container at zero-based position_or_key.
| on | Returns the specified date/time in integer system representation.
| or | Calculate whether any argument evaluates to true
| packagesmatching | Return a data container with the list of installed packages matching the parameters.
| packageupdatesmatching | Return a data container with the list of available packages matching the parameters.
| parseintarray | Parses up to maxentries values from the first maxbytes bytes in string input and populates array. Returns the dimension.
| parsejson | Parses JSON data directly from an inlined string and returns the result as a data variable
| parserealarray | Parses up to maxentries values from the first maxbytes bytes in string input and populates array. Returns the dimension.
| parsestringarray | Parses up to maxentries values from the first maxbytes bytes in string input and populates array. Returns the dimension.
| parsestringarrayidx | Populates the two-dimensional array array with up to maxentries fields from the first maxbytes bytes of the string input.
| parseyaml | Parses YAML data directly from an inlined string and returns the result as a data variable
| peerleader | Returns the current host's partition peer leader.
| peerleaders | Returns a list of partition peer leaders from a file of host names.
| peers | Returns the current host's partition peers (excluding it).
| product | Returns the product of the reals in list.
| randomint | Returns a random integer between lower and up to but not including upper.
| read[int|real|string]list
| readcsv | Parses CSV data from the first 1 MB of file filename and returns the result as a data variable.
| readdata | Parses CSV, JSON, or YAML data from file filename and returns the result as a data variable.
| readfile | Returns the first maxbytes bytes from file filename. When maxbytes is 0, the maximum possible bytes will be read from the file (but see Notes below).
| readintarray | Populates array with up to maxentries values, parsed from the first maxbytes bytes in file filename.
| readjson | Parses JSON data from the first maxbytes bytes of file filename and returns the result as a data variable.
| readrealarray | Populates array with up to maxentries values, parsed from the first maxbytes bytes in file filename.
| readstringarray | Populates array with up to maxentries values, parsed from the first maxbytes bytes in file filename.
| readstringarrayidx | Populates the two-dimensional array array with up to maxentries fields from the first maxbytes bytes of file filename.
| readtcp | Connects to tcp port of hostnameip, sends sendstring, reads at most maxbytes from the response and returns those.
| readyaml | Parses YAML data from the first maxbytes bytes of file filename and returns the result as a data variable.
| regarray | Returns whether array contains elements matching the anchoredregular expression regex.
| regcmp | Returns whether the anchored regular expression regex matches the string.
| regextract | Returns whether the anchored regex matches the string, and fills the array backref with back-references.
| registryvalue | Returns the value of valueid in the Windows registry key key.
| regldap | Returns whether the regular expression regex matches a value item in the LDAP search.
| regline | Returns whether the anchored regular expression regex matches a line in file filename.
| reglist | Returns whether the anchored regular expression regex matches any item in list.
| remoteclassesmatching | Reads persistent classes matching regular expression regex from a remote CFEngine server server and adds them into local context with prefix prefix.
| remotescalar | Returns a scalar value identified by id from a remote CFEngine server. Communication is encrytped depending on encrypt.
| returnszero | Runs command and returns whether it has returned with exit status zero.
| reverse | Reverses a list.
| rrange | Define a range of real numbers for CFEngine internal use.
| selectservers | Returns the number of tcp servers from hostlist which respond with a reply matching regex to a query send to port, and populates array with their names.
| shuffle | Return list shuffled with seed.
| some | Return whether any element of list matches the Unanchored regular expression regex.
| sort | Returns list sorted according to mode.
| splayclass | Returns whether input's time-slot has arrived, according to a policy.
| splitstring | Splits string into at most maxent substrings wherever regex occurs, and returns the list with those strings.
| storejson | Converts a data container to a JSON string.
| strcmp | Returns whether the two strings string1 and string2 match exactly.
| strftime | Interprets a time and date format string at a particular point in GMT or local time using Unix epoch time.
| string_downcase | Returns data in lower case.
| string_head | Returns the first max bytes of data.
| string_length | Returns the byte length of data.
| string_mustache | Formats a Mustache string template into a string, using either the system datastate() or an explicitly provided data container.
| string_reverse | Returns data reversed.
| string_split | Splits string into at most maxent substrings wherever regex occurs, and returns the list with those strings.
| string_tail | Returns the last max bytes of data.
| string_upcase | Returns data in uppercase.
| sublist | Returns list of up to max_elements of list, obtained from head or tail depending on head_or_tail.
| sum | Return the sum of the reals in list.
| translatepath | Translate separators in path from Unix style to the host's native style and returns the result.
| unique | Returns list of unique elements from list.
| usemodule | Execute CFEngine module script module with args, and return whether successful.
| userexists | Return whether user name or numerical id exists on this host.
| variablesmatching | Return the list of variables matching name and any tags given. Both name and tags are regular expressions.
| variance | Return the variance of the numbers in list.
|=======================================================================================
